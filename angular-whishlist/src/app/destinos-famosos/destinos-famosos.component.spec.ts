import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinosFamososComponent } from './destinos-famosos.component';

describe('DestinosFamososComponent', () => {
  let component: DestinosFamososComponent;
  let fixture: ComponentFixture<DestinosFamososComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DestinosFamososComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinosFamososComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
