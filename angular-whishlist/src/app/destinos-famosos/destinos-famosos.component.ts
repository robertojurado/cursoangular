import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-destinos-famosos',
  templateUrl: './destinos-famosos.component.html',
  styleUrls: ['./destinos-famosos.component.css']
})
export class DestinosFamososComponent implements OnInit {

  destinosFamosos = ['Barranquilla', 'Bucaramanga', 'Cali', 'Cartagena', 'Manizales', 'Medellin'];	

  constructor() { }

  ngOnInit(): void {
  }

}
